import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup,NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { User } from '../services/user/UserTypes';
import {Router} from '@angular/router';
import { DataService } from '../services/data.service'
import {UsersService} from '../services/user/users.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})


export class AuthPage implements OnInit {
  model: any = {};

  public listeUsers: Observable<User[]>;
  public form: FormGroup;
  public formulaire = {email: '', password: ''};
  public message: string;
  public AuthGuard;
  public url: string;
  public user: any;




  
  constructor( 
      public formBuilder: FormBuilder,
      public  UsersService:UsersService,
      public navCtrl: NavController,
      public HttpClient:HttpClient,
      private router: Router,
      private dataService: DataService
       ){                    
       }

   logForm(form: NgForm){ 

   
      console.log(form);

    if (form.status === "VALID" && form.submitted)
    {
            this.UsersService.getUser(form.value.email,form.value.password)
            .subscribe
            (user =>
          {
              this.user = user;
              this.dataService.setData('user',this.user );
              this.router.navigateByUrl('/home');            
          });
      } 
      else
      {
        this.message = 'Erreur';
      }

  
    
    }

  ngOnInit() {
  }




}
