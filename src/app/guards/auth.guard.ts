import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router,ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate  {
  public userAuthenticated;
 // Dans le constructeur on déclare notre variable de routage
 constructor(private router: Router,private ActivatedRoute: ActivatedRoute) {
  // this.userAuthenticated = this.ActivatedRoute.snapshot.paramMap.get('bool')
  this.userAuthenticated =true;
}

  canActivate( route: ActivatedRouteSnapshot,state: RouterStateSnapshot):boolean 
    {

    // let userAuthenticated = false; // Pour le moment nous allons garder cette valeur à false



    if (this.userAuthenticated) {
      return true;
    } else {
     // return false;
      this.router.navigate(['/auth']);
    }
  }
}
