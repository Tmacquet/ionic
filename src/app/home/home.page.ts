import { Component } from '@angular/core';
import { ToastController,AlertController,PopoverController, IonDatetime} from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router} from '@angular/router';
import { DataService } from '../services/data.service'
import {UsersService} from '../services/user/users.service';
import {RendezVousServiceService} from '../services/rendezVous/rendez-vous-service.service';
import{RendezVous} from '../services/rendezVous/RendezVousTypes';
import { format } from 'url';



@Component({
  selector: 'app-home',
  templateUrl: '/home.page.html',
  styleUrls: ['/home.page.scss'],
})


export class HomePage {

  data: any;

    public RendezVous: any;
    // Observable<RendezVous>;
    public user={
      id: "",
      nom: "",
      prenom: ""
    };
    public dateAujourdhui :any;
    public dateDemain:any;
    date: Date;
    public id;
    public formulaire: Array<[]> ;
    public message: string ;
    public idUser: number;
    public commercial :any;
    public infoClient: object;
    public dataTableau ={
      date:"",
      heure:"",
      numeroCommande:"",
      user:""
    };
    public today  ;

  constructor(
      public rendezVousService: RendezVousServiceService,
      public userService: UsersService,
      public toastController: ToastController,
      public popoverCtrl: PopoverController,
      public alertController: AlertController,
      private route: ActivatedRoute,  
      public HttpClient:HttpClient,
      private router: Router,
      private dataService: DataService   
    )
    {
      console.log("allcountry constuctor are called");     
    }
    ngOnInit() 
    {
        if (this.dataService.getData('user')) {
          this.commercial = this.dataService.getData('user');
          this.user.nom=this.commercial[0]['nom'];
          this.user.prenom=this.commercial[0]['prenom'];
          this.user.id=this.commercial[0]['id'];         
          this.rendezVousService.getRendezVous(this.user.id)
          .subscribe
          (rendezVous =>
          {
            this.today = new Date();
            this.dateAujourdhui =  this.today.getFullYear()+'-'+ (this.today.getMonth() + 1 )+'-'+this.today.getDate();
            this.dateDemain=  this.today.getFullYear()+'-'+ (this.today.getMonth()+ 1 )+'-'+ (this.today.getDate() + 1 );
            this.RendezVous = rendezVous;
          }); 
        }
    }

    donneesFiche(client) {
      this.infoClient = {client};
      this.dataService.setData( 'client', this.infoClient);
      this.router.navigateByUrl('/fiche');
    }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your settings have been saved.',
      duration: 2000
    });
    toast.present();
  }

    async presentToastWithOptions(date , heure) {
      const toast = await this.toastController.create({
        position: 'top',
        buttons: [
          {
            icon: 'close-circle',
            text: 'rendez-vous à ' + heure + ' le ' + date ,
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }
    async presentPopover() {
      const alert = await this.alertController.create({
        header: 'Ajouter un rendez-vous',
        inputs: [
          {
            placeholder: 'date',
            label: 'date',
            type: 'date'
          },
          {
            placeholder: 'heure',
            label: 'heure',
            type: 'time'
          },
          {
            placeholder: 'nom de la personne ',
            label: 'name',
            type: 'search'
          },


        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Okay',
            handler: (data) => {
              this.dataTableau.date = data[0] ;
              this.dataTableau.heure = data[1];
              this.dataTableau.numeroCommande =data[2];
              this.dataTableau.user =  this.user.id;           
              this.rendezVousService.postRendezVous(this.dataTableau)
              .subscribe
              (rendezVous =>
              {    
              });
            }
          }
        ]
      });

      await alert.present();
    }



}
