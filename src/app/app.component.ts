import { Component } from '@angular/core';

import {AlertController, Platform, PopoverController, ToastController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import {RendezVousServiceService} from './services/rendezVous/rendez-vous-service.service';
import {UsersService} from './services/user/users.service';
import {HttpClient} from '@angular/common/http';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Profil',
      url: '/profil',
      icon: 'home'
    },
    {
      title: 'Catalogues',
      url: '/catalogues',
      icon: 'home'
    },
    {
      title: 'A propos',
      url: '/apropos',
      icon: 'home'
    },
    {
      title: 'Deconnexion',
      url: '/auth',
      function: this.logout(),
      icon: 'log-out'
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private activatedRouter: ActivatedRoute,
    public http: HttpClient,
    rendezVousService: RendezVousServiceService,
    userService: UsersService,
    public toastController: ToastController,
    public popoverCtrl: PopoverController,
    public alertController: AlertController,
    private router: Router,
    private dataService: DataService,
  ) {
    this.initializeApp();
    console.log(this.dataService);


  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['/auth']);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
