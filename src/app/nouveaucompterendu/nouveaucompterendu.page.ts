import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../services/data.service';
import {CompteRenduService} from '../services/CompteRendu/compte-rendu.service'

@Component({
  selector: 'app-nouveaucompterendu',
  templateUrl: './nouveaucompterendu.page.html',
  styleUrls: ['./nouveaucompterendu.page.scss'],
})
export class NouveaucompterenduPage implements OnInit {
  form ={
    date:'',
    description:''

  };
  dataTableau={
    date:'',
    description:'',
    id_client:'',
    id_commande:'',
    id_user:''
  }
  public client:any;
  public idcommerciale:any;
  public message :any;

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    public HttpClient: HttpClient,
    private dataService: DataService,
    private CompteRenduService:CompteRenduService
    ) { }

  ngOnInit() {

  }

  logForm(form)
   {
  
      if (form.status === "VALID")
      {
          if (this.dataService.getData('user')) {
              this.dataTableau.date = form.date ;
              this.dataTableau.description = form.description;  
              this.dataTableau.id_client= this.dataService.getData('client').client.id;
              this.dataTableau.id_commande=this.dataService.getData('listeArticle')[0].id_commande;
              this.dataTableau.id_user=this.dataService.getData('user')[0].id; 
              this.CompteRenduService.postCompteRendu(this.dataTableau)
              .subscribe
              (compteRendu =>              
              {    
                this.dataService.setData( 'message',compteRendu);
                this.router.navigateByUrl('/details');
              });
        }
      }
  }
}
