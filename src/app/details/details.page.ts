import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  public id;
  public message :any;
  public infoClient : any;
  public articles : any;
  public total : any;
   
  constructor
  (
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private dataService: DataService,
    private route: ActivatedRoute, 
  )
  { 
    if (this.dataService.getData('listeArticle')) {
      this.articles = this.dataService.getData('listeArticle');
    }



  }

  ngOnInit() {
    this.articles.forEach(element => {
      this.total = element.prix *element.quantite; 
    });
    if (this.dataService.getData('message')) {
      this.message = this.dataService.getData('message');    
    }
   

  }
  nouveaucompterendu() {

    const $donnees = this.dataService.getData('client');
    this.dataService.setData('donnees', $donnees );
    this.router.navigateByUrl('/nouveaucompterendu');
    
  }


  

}
