import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes , ActivatedRoute } from '@angular/router';
import { DataResolverService } from './resolver/data-resolver.service';

// On appelle notre gardien ici
import { AuthGuard } from './guards/auth.guard';




const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'home',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './home/home.module#HomePageModule',
    //  () => import('./home/home.module').then(m => m.HomePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'profil',
    resolve: {
      special: DataResolverService
    },
    loadChildren:'./profil/profil.module#ProfilPageModule',
    //  () => import('./profil/profil.module').then( m => m.ProfilPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogues',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./catalogues/catalogues.module').then( m => m.CataloguesPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'apropos',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./apropos/apropos.module').then( m => m.AproposPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'ajout',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./ajout/ajout.module').then( m => m.AjoutPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'fiche',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './fiche/fiche.module#FichePageModule',
    // () => import('./fiche/fiche.module').then( m => m.FichePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'ajoutercommande',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./ajoutercommande/ajoutercommande.module').then( m => m.AjoutercommandePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'details',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './details/details.module#DetailsPageModule',
    // () => import('./details/details.module').then( m => m.DetailsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'nouveaucompterendu',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './nouveaucompterendu/nouveaucompterendu.module#NouveaucompterenduPageModule',
    //  () => import('./nouveaucompterendu/nouveaucompterendu.module').then( m => m.NouveaucompterenduPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'localisation',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./localisation/localisation.module').then( m => m.LocalisationPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthPageModule)
  },
  {
    path: 'deconnexion',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./deconnexion/deconnexion.module').then( m => m.DeconnexionPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),

  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
