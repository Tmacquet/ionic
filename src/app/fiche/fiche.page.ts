import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import {Router,NavigationExtras,ActivatedRoute} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../services/data.service';
import {ArticleCommandeService} from '../services/ArticleCommande/article-commande.service';
import{CommandeArticle} from '../services/ArticleCommande/ArticleCommandeTypes';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fiche',
  templateUrl: './fiche.page.html',
  styleUrls: ['./fiche.page.scss'],
})
export class FichePage implements OnInit 
{

  public CommandeArticle: Observable<CommandeArticle[]>;
  public commandes;
  public listeArticle ;
  public id;
  public datas: any;
  public donnes: any;
  public infoClient : any;
  public client = {
      adresse: "",
      date_inscription: "",
      email: "",
      id: "",
      nb_commande: "",
      codePostal:"",
      nom: "",
      pays: "",
      prenom: "",
      telephone: "",
      ville: ""
  }



  constructor(
    private callNumber: CallNumber, 
     public articleCommandeService: ArticleCommandeService,
     public HttpClient: HttpClient,
     private router: Router,
     private dataService: DataService,
     private route: ActivatedRoute, 
     )
  {
    
    if (this.dataService.getData('client')) {
      this.infoClient = this.dataService.getData('client');
      this.client.id=this.infoClient['client']['id'];
      this.client.nom= this.infoClient['client']['nom'];
      this.client.prenom=this.infoClient['client']['prenom'];
      this.client.email=this.infoClient['client']['email'];
      this.client.telephone=this.infoClient['client']['telephone'];
      this.client.adresse =this.infoClient['client']['adresse'];
      this.client.codePostal =this.infoClient['client']['codePostal'];
      this.client.ville=this.infoClient['client']['ville'];
      this.client.pays=this.infoClient['client']['pays'];
      this.client.nb_commande=this.infoClient['client']['nb_commande'];
      this.articleCommandeService.getArticlesCommandeById(this.infoClient['client']['id']).subscribe
      (commandes =>
      {
        this.commandes = commandes;
      });
    }
  }

  ngOnInit() 
  {   
  }


  call() {
    this.callNumber.callNumber('0604138940', true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  details(numero) {
      this.articleCommandeService.getArticleCommandeByNumero(numero).subscribe
      (listeArticle =>
      {
        this.listeArticle = listeArticle;
        this.dataService.setData('listeArticle',this.listeArticle );
        this.router.navigateByUrl('/details');
     });
  }

  localisation() {
    this.router.navigate(['/localisation']);
  }


}
